<div class="right">
    <?PHP

    function validate_path($filename) {
        // $filename = str_replace("../", "", $filename);
        // $filename = str_replace('..\\', "", $filename);

        while (true) {
            $filename_old = $filename;
            $filename = str_replace("../", "", $filename);
            if ($filename_old == $filename)
                break;
        }

        while (true) {
            $filename_old = $filename;
            $filename = str_replace('..\\', "", $filename);
            if ($filename_old == $filename)
                break;
        }
        return $filename;
    }

    $sorting = "ascending order";
    if (isset($_COOKIE['sorting'])) {
        $sorting = $_COOKIE["sorting"];
    }

    $template = "default.php";
    if (isset($_COOKIE['template'])) {
        $templateName = $_COOKIE['template'];
        if ($templateName == "dark") {
            $template = "dark.php";
        } else {
            $template = "default.php";
        }
    }
    $templatePath = "Path Traversal/_template/".$template;
    include ($templatePath);

    $title = "";
    $text = "";

    if (isset($_POST['add'])) {	
        $title = $_POST['title'];
        $text = $_POST['text'];
        if ($title != "" && $text != "") {
            $filename = "Path Traversal/_textfiles/".$title.".txt";
            if (file_exists($filename)) {
                echo "Please use unique titles!<br/><br/>";
            } else {
                file_put_contents($filename, $text);
                echo "The file was saved successfully!<br/><br/>";
            }
        } else {
            echo "Please enter a title and text!<br/><br/>";
        }
    }

    if (isset($_POST['save'])) {	
        $title = $_POST['title'];
        $original_title = $_POST['original_title'];
        $text = $_POST['text'];
        if ($title != "" && $text != "" && $original_title != "") {
            $filename = "Path Traversal/_textfiles/".$title.".txt";
            if (file_exists($filename) && $title != $original_title) {
                echo "Please use unique titles!<br/><br/>";
            } else {
                if ($title != $original_title) {
                    $original_filename = "Path Traversal/_textfiles/".$original_title.".txt";
                    rename($original_filename, $filename);
                }
                file_put_contents($filename, $text);
                echo "The file was saved successfully!<br/><br/>";
            }
        } else {
            echo "Please enter a title and text!<br/><br/>";
        }
    }

    if (isset($_POST['delete']) && isset($_POST['original_title']) && $_POST['original_title'] != "") {	
        $original_title = $_POST['original_title'];
        $filename = "Path Traversal/_textfiles/".$original_title.".txt";
        unlink($filename);
        echo "The file was deleted successfully!<br/><br/>";
    }

    $add_new_file_mask = false;
    if (isset($_GET['f'])) {
        // Vulnerability
        // Change the GET parameter to: f=....\/_config.json
        $filename = "Path Traversal/_textfiles/".$_GET['f'];
        $filename = validate_path($filename);
        // echo $filename;

        if (file_exists($filename)) {
            $text = file_get_contents($filename);
            $filename_without_extension = basename($filename, '.txt');
            ?>
            <form action="" method="post">
                Title:<br/>
                <input type="text" name="title" size="24" maxlength="24" value="<?PHP echo $filename_without_extension; ?>"><br/><br/><br/>

                Text:<br/>
                <textarea name="text" rows="20" cols="65"><?PHP echo $text; ?></textarea><br/>

                <input type="hidden" name="original_title" value="<?PHP echo $filename_without_extension; ?>">
                <input type="submit" name="save" value="Save">
                <input type="submit" name="delete" value="Delete">
            </form>
        
            <?PHP
        } else {
            $add_new_file_mask = true;
        }
    } else {
        $add_new_file_mask = true;
    }
    
    if ($add_new_file_mask == true) {
        ?>
        <form action="" method="post">
            Title:<br/>
            <input type="text" name="title" size="24" maxlength="24" value="<?PHP echo $title; ?>"><br/><br/><br/>

            Text:<br/>
            <textarea name="text" rows="20" cols="65"><?PHP echo $text; ?></textarea><br/>

            <input type="submit" name="add" value="Add">
        </form>
        <?PHP
    }
    ?>
</div>

<div class="left">
    <input type="submit" onclick="location.href='index.php?page=Credentials%20Steal%203&folder=Path%20Traversal';" value="Add new file"/>
    <hr>
    <select size="1" name="sorting" id="sorting">
        <option <?PHP if ($sorting == "descending order") {echo "selected";} ?>>descending order</option>
        <option <?PHP if ($sorting == "ascending order") {echo "selected";} ?>>ascending order</option>
    </select>
    <ul style="text-align: left;">
        <?PHP
        $directory = 'Path Traversal/_textfiles';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));

        if ($sorting == "ascending order") {
            sort($scanned_directory);
        } else {
            rsort($scanned_directory);
        }

        foreach ($scanned_directory as $key => $filename) {
            $filename_without_extension = basename($filename, '.txt');
            echo "<li><a href='index.php?page=Credentials%20Steal%203&folder=Path%20Traversal&f=".$filename."'>".$filename_without_extension."</a></li>";
        }
        ?>
    </ul>
</div>
<br/>
Theme: 
<select size="1" name="template" id="template">
    <option value="default" <?PHP if ($template == "default") {echo "selected";} ?>>Default</option>
    <option value="dark" <?PHP if ($template == "dark") {echo "selected";} ?>>Dark</option>
</select>

<script>
    let sorting_element = document.getElementById('sorting');
    sorting_element.addEventListener ("change", function () {
        setCookie("sorting", sorting_element.value, 365);
        location.reload();
    });

    let template_element = document.getElementById('template');
    template_element.addEventListener ("change", function () {
        setCookie("template", template_element.value, 365);
        location.reload();
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
</script>