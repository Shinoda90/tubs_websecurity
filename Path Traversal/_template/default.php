<style>
.left {
    overflow: hidden;
    min-height: 500px;
    border: 1px solid #ccc;
    color: #111;
    background: white;
}

a {
    color: #111;
}

input[type=text] {
    background: #eee !important;
    color: #111 !important;
}

hr {
  border: 1px solid #ccc;
  background-color: #ccc;
  color: #ccc;
}

textarea {
    background: #eee;
    border-color: #ccc;
    color: #111;
}

.right {
    float: right;
    width: 75%;
    min-height: 500px;
    margin-left: 10px;
    border: 1px solid #ccc;
    color: #111;
    background: white;
}
</style>