<style>
.left {
    overflow: hidden;
    min-height: 500px;
    border: 1px solid #444;
    color: #eee;
    background: black;
}

a {
    color: #eee;
}

input[type=text] {
    background: #222 !important;
    color: #eee !important;
}

hr {
  border: 1px solid #444;
  background-color: #444;
  color: #444;
}

textarea {
    background: #222;
    border-color: #444;
    color: #eee;
}

.right {
    float: right;
    width: 75%;
    min-height: 500px;
    margin-left: 10px;
    border: 1px solid #444;
    color: #eee;
    background: black;
}
</style>