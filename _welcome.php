
<h1> Willkommen </h1>
<div style="text-align: justify;">
<div style="display: inline-block; text-align: justify;">

<p><strong>Auf dieser Seite finden Sie einige Hinweise, die Ihnen den Umgang mit unserer Seite erleichtern sollen.</strong><br><br>

Mithilfe dieses Frameworks sollen Sie Ihr Verständnis für ausgewählte Vorlesungsinhalte festigen, indem Sie sich selbst an Aufgaben 
zu den vorgestellten Themen versuchen.<br><br>
Anhand unserer Beispielschwachstellen CSRF, XSS und SQLI werden hier grundlegende Funktionen und Besonderheiten des Frameworks 
verdeutlicht.

<center><h2> Anleitung </h2></center>

<center><h3>Einbindung von Seiten</h3></center>

Um eine neue Seite einzubinden, erstellt man zunächst einen neuen Ordner in dem gleichen Ordner in dem sich auch "index.php" befindet.
Erstellte Ordner werden dann als Drop-Down-Menüs in der Navigationsleiste angezeigt. Die Menüpunkte werden hierbei durch die 
php-Dateien in den entsprechenden Ordnern festgelegt. Dateien und Ordner, die mit einem Unterstrich beginnen, werden nicht angezeigt. 
Dateien, die im selben Ordner wie "index.php" sind, werden ebenfalls nicht angezeigt. Es wird außerdem eine Session gestartet, sodass 
diese bei Bedarf in der eingebundenen php-Datei genutzt werden kann.<br><br>
Beispielsweise würde der Ordner "_ignore" nicht in der Navigation angezeigt werden. Genauso wird die Datei "_CSRF (Trap).php" im 
Ordner CSRF nicht in der Navigation angezeigt, jedoch kann der Inhalt der Datei weiterhin normal unter 
"localhost/CSRF/_CSRF (Trap).php" aufgerufen werden.<br><br>
Bei der Auswahl einer Seite in der Navigation werden Ordner und Dateiname als GET-Parameter an "index.php" übergeben und die Seite 
wird mittels include() unterhalb der Navigation eingebunden. Dabei muss die angezeigte Datei keine vollständige html-Struktur haben, 
da dies durch die Einbindung in die Index-Seite gegeben ist. Die Startseite "_welcome.php" wird ebenfalls so eingebunden.<br><br>
<br>

<center><h3>Datenbanknutzung</h3></center>

Für die Nutzung von Datenbanken gibt es die Möglichkeit, sql-Dateien aus den Ordnern einzulesen. Im Ordner "SQL Injection" befindet 
sich hierfür die sql-Datei "SQLi.sql", welche Statements für die bedingte Erstellung und anschließende Verwendung einer Datenbank 
und Tabelle über den bereitgestellten PDO gibt. Die Ordner werden in alphabetischer Reihenfolge als Statements von dem PDO in der 
Variable "$db" ausgeführt. Über diese Variable kann dann in den eingebundenen php-Dateien weiter auf die vorbereiteten Datenbanken 
zugegriffen werden. Diese sql-Datei wird dann bei Einbindung einer php-Datei aus dem selben Ordner (in diesem Fall "SQLi.php") 
automatisch ausgeführt und im Anschluss ohne weitere Deklaration genutzt. Auch hier werden wieder sql-Dateien, welche mit einem Unterstrich beginnen, nicht automatisch ausgeführt (z.B. für eigene Verwendung 
an anderer Stelle).
<br>

<center><h3>Formulare</h3></center>

Bei GET-Formularen müssen GET-Parameter auf der gleichen Seite beibehalten werden, da sonst keine Einbindung mehr stattfindet und die Startseite diese Parameter nicht nutzen würde.
Beispielsweise wird dies in der Datei "CSRF/CSRF (Target).php" verwendet. Dort werden über den php-Block in den Zeilen 22 - 30 sämtliche GET-Parameter für dieses Formular übernommen, wobei verwendete Parameter ignoriert werden. 
Für POST-Formulare gilt dies nicht, da dort GET-Parameter erhalten werden.
</p>
</div>
</div>