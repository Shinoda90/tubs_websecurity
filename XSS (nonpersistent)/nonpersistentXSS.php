
<?php
	if(isset($_GET['input'])) {
		echo '<p id="p01">Sie haben folgendes eingegeben:</p>
		'.$_GET['input'];
	} else {
		echo '<p id="p01">Geben Sie etwas ein!</p>';
	}
?>
<form action="" method="get">
	<?php
	foreach($_GET as $name => $value) {
		if ($name != 'input') {
			$name = htmlspecialchars($name);
			$value = htmlspecialchars($value);
			echo '<input type="hidden" name="'. $name .'" value="'. $value .'">';
		}
	}
	?>
	<p>Ihre Eingabe: <input type="text" name="input" /></p>
	<p><input type="submit" /></p>
</form>