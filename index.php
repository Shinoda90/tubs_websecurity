<html>
    <head>
		<style>
		<?php include 'style.css'; ?>
		</style>
		<?php 
		if(isset($_GET['folder']) AND isset($_GET['page'])) {
			echo '<title> File: ' . $_GET['page'] . '</title>';
		} else {
			echo '<title> Start </title>';
		}	
		
		?>
	</head>
	
	<body>
		<?php session_start();?>
		<div class="topnav">
				
			
		
			<div class="topnav">
			<?php	
				if(isset($_GET['folder']) AND isset($_GET['page'])) {
					echo '<a href="index.php">Startseite</a>';
				} else {
					echo '<a class="active" href="index.php">Startseite</a>';
				}
				
	
			#$include_folders = array("exercises" => "Aufgaben", "pages" => "Seiten");
			
			$oberverzeichnis = openDir(".");
			$sqlfiles = array();
			
			while ($folder = readDir($oberverzeichnis)) {
			#foreach ($include_folders AS $folder => $foldername) {
			
			
			
			// Öffnet ein Unterverzeichnis mit dem Namen "."
			if ($verzeichnis = @openDir($folder) AND $folder != ".." AND $folder != "." AND !(strpos($folder, "_") === 0)) {
				
			echo '<div class="dropdown">
			<button class="dropbtn">'.$folder.' 
				<i class="fa fa-caret-down"></i>
			</button>
			<div class="dropdown-content">';
				// Verzeichnis lesen
				while ($file = readDir($verzeichnis)) {
				// Höhere Verzeichnisse nicht anzeigen!
					if ($file != "." && $file != "..") {
					// Dateityp filtern. Es werden nur .php-Dateien angezeigt
						if (strpos($file, ".php") AND !(strpos($file, "_") === 0)) {
							// Dateiendung vom Dateinamen filtern
							$name = explode(".", $file);
							if((isset($_GET['folder']) AND ($_GET['folder'] == $folder)) AND (isset($_GET['page']) AND $_GET['page'] == $name[0])) {
								
								echo '<a class="active" href="./index.php?page=' . $name[0] . '&folder=' . $folder . '">' . $name[0] . '</a>';
							} else {
									 
								echo '<a href="./index.php?page=' . $name[0] . '&folder=' . $folder . '">' . $name[0] . '</a>';
							}
						}
						if (strpos($file, ".sql") AND isset($_GET['folder']) AND ($_GET['folder'] == $folder)) {
							#sql dateien zur einbindung merken. -> sollte nur eine sein, da keine reihenfolge beachtet werden kann?
							
							$sqlfiles[] = $file;
						}
					}
				}
				closeDir($verzeichnis); // Verzeichnis schließen
				
				echo '</div>
				</div>';
				}
			}
			closeDir($oberverzeichnis);
			?>
			</div>
		</div>
		
		<div id="des">
		<?php
		if(isset($_GET['folder']) AND isset($_GET['page'])) {
			if(file_exists('./' . $_GET['folder'] . '/' . $_GET['page'] . '.php')) {
				if (!(empty($sqlfiles))) {
					sort($sqlfiles);
					$db = new PDO('mysql:host=localhost;charset=UTF8', 'root', '');
					foreach ((array) $sqlfiles AS $sqlfile) {
						if (!(strpos($sqlfile, "_") === 0)) {	
							$sql = file_get_contents('./' . $_GET['folder'] . '/' . $sqlfile);
							
							$db->exec($sql);
							$sql = null;
						}
					}
				}
				
				include('./' . $_GET['folder'] . '/' . $_GET['page'] . '.php');
			} else {
				echo '<br>Keine solche Datei gefunden!';
			}
		} else {
			include '_welcome.php';
		}
		?>
		</div>
	</body>
</html>
