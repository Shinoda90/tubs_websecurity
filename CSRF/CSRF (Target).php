<?php
if(isset($_GET['var'])) {
	$_SESSION['wert'] = $_GET['var'];
}

if(isset($_SESSION['wert'])) {
	echo '<p>Ihr wichtiger Wert ist: '. $_SESSION['wert'] . '</p>';	
} else {
	echo '<p>Bitte setzen Sie einen wichtigen Wert!</p>';
}

if(isset($_GET['close'])) {
	if($_GET['close'] == true) {
		session_destroy();
	}	
}
		
?>

<form action="" method="get">

	<?php
	foreach($_GET as $name => $value) {
		if ($name != 'var' AND $name != 'close') {
			$name = htmlspecialchars($name);
			$value = htmlspecialchars($value);
			echo '<input type="hidden" name="'. $name .'" value="'. $value .'">';
		}
	}
	?>
	
	<p>Wert setzen: <input type="text" name="var" /></p>
	<input type="radio" name="close" value="true"> Session schließen
	<p><input type="submit" /></p>
</form>