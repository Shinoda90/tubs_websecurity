Webpage-Framework f�r die Vorlesung Websicherheit.

Zur Verwendung dieses Tools wird ein Webserver mit aktueller php-Version und ein Datenbankserver ben�tigt.
F�r die lokale Verwendung zur Aufgabenbearbeitung empfehlen sich Paket-L�sungen, welche diese Komponenten beinhalten.
Als Beispiel kann hier Xampp (Plattform�bergreifend) genannt werden, welches auf www.apachefriends.org als Freeware erh�ltlich ist.
Xampp bietet eine einfache Installation und Oberfl�che, in der nur 2 Klicks n�tig sind, um den Apache-Webserver und den Datenbankserver zu starten.

Der Inhalt der Zip-Datei muss dann im Documents-Root des Webservers entpackt werden. (Bei Xampp: Ordner "htdocs" im Installationsordner)
Der Webzugriff auf die Seiten erfolgt nach Start des Servers lokal �ber die Index-Seite mit einen Browser unter der Adresse "localhost/index.php".
Hier werden auch alle einzubindenden Seiten in einer Navigation angezeigt und k�nnen zun�chst ohne genauere Ordnerkenntnis aufgerufen werden.
Es stehen einige Beispielseiten f�r Schwachstellen bereit, welche als Hilfe zur Nutzung und eigenen Bearbeitung dienen k�nnen.
