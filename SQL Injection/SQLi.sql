CREATE DATABASE IF NOT EXISTS sqlidb;
ALTER DATABASE sqlidb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use sqlidb;
CREATE TABLE IF NOT EXISTS guests (id INT(11) AUTO_INCREMENT PRIMARY KEY, privatekey INT(11) NOT NULL, firstname VARCHAR(50) NOT NULL, lastname VARCHAR(250) NOT NULL);
